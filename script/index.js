

// Створіть функцію user, зробіть так, щоб функція виводила в консоль контекст

// Створіть об'єкт userOne, додайте до нього властивості ім'я, прізвище, вік та функцію, яка виводитиме Привіт! Я ім'я + прізвище Виведіть ім'я та прізвище користувача з об'єкта userOne.

// Зробіть так, щоб контекст у методі об'єкта userOne був глобальним об'єктом window.

// Створіть ще один об'єкт userTwo і заповніть його тими самими властивостями, але без методу.

// Виведіть інформацію з об'єкта userTwo, використовуючи метод об'єкта userOne.

function user () {
    console.log(this);
}

userOne = {
    name: 'Pavlo',
    surname: 'Solomatin',
    age: 36,
    show () {
        console.log(`Привіт! Я ${this.name} ${this.surname}!`)
    }
}

userTwo = {
    name: 'Pavlo',
    surname: 'Solomatin',
    age: 36,
}

userOne.show();

userOne.show.bind(userTwo)()                                    //метод подмены bind, apply, call

class Card {
    constructor() {

    }
    getObj(obj) {
        let newObj = [];
        obj.forEach((elem) => {
            newObj.push(
                `<div class="card" >
                    <img src="${elem.image}" class="card-img-top"
    alt="...">
                        <div class="card-body">
                            <h5 class="card-title">${elem.title}</h5>
                            <p class="card-text">${elem.description}</p>
                            <a href="#" class="btn btn-primary">${elem.price}</a>
                        </div>
                </div>`
            );
        })
        return newObj;
    }
}
const newCard = new Card();
document.write(newCard.getObj(data));